== 功能介绍

目前支持功能:

- <<project_manage>>
- <<env_manage>>
- <<conf_manage>>
- <<app_manage>>
- <<notice_manage>>
- <<client_connect>>
- <<keyboard_shortcut>>

[#project_manage]
=== 项目管理

项目管理是为了满足多项目共用同一配置中心而开发的。

由于 <<env_manage>> 、 <<conf_manage>> 、 <<app_manage>> 、<<notice_manage>> 功能都是基于 `项目` 衍生出来的管理模块，需要依赖项目，所以进入配置中心首先就要创建项目，并设置当前项目。

项目名只作为备注使用，便于使用者查看，可以使用中文，甚至都没有加入重复校验。

NOTE: 项目编号只能是数字、字母和英文半角符号的减号(`-`)，并且第一个字符必须是英文（其它编号也是如此），目的是为了满足SpringCloudConfig默认的规则，避免出现识别错误BUG。


[#env_manage]
=== 环境管理

环境管理用来区分同一项目中不通的运行环境，例如 `本地开发环境`、`本地测试环境`、`用户测试环境`、`生产环境` 等。

环境名只作为备注使用，便于使用者查看，可以使用中文，甚至都没有加入重复校验。

NOTE: 环境编号只能是数字、字母和英文半角符号的减号(`-`)，并且第一个字符必须是英文（其它编号也是如此），目的是为了满足SpringCloudConfig默认的规则，避免出现识别错误BUG。

[#conf_manage]
=== 配置管理

配置就是应用最终使用的参数项，在客户端应用成功连接到配置中心后，即可读取配置中心中配置好的参数。

配置和应用的关系是多对多的，并非一个应用只能读取一个配置，而是可以读取多个。例如 `测试应用1` 可以同时读取 `测试应用1专用配置` 和 `测试环境数据库通用配置` 两个配置。它们之间通过应用详情页面进行关联，没有关联的配置不会被应用读取到。

[#app_manage]
=== 应用管理

应用就是实际开发中编写代码的服务模块，既可以是单体应用也可以是微服务中的某个服务。

应用名只作为备注使用，便于使用者查看，可以使用中文，甚至都没有加入重复校验。

应用和配置的关系是多对多的，并非一个应用只能读取一个配置，而是可以读取多个。例如 `测试应用1` 可以同时读取 `测试应用1专用配置` 和 `测试环境数据库通用配置` 两个配置。它们之间通过应用详情页面进行关联，没有关联的配置不会被应用读取到。

NOTE: 应用编号只能是数字、字母和英文半角符号的减号(`-`)，并且第一个字符必须是英文（其它编号也是如此），目的是为了满足SpringCloudConfig默认的规则，避免出现识别错误BUG。

[#client_connect]
=== 连接到配置中心

配置中心是负责管理参数的，而这些参数真正的使用者是客户端。

要想连接到配置中心，客户端首先需要添加依赖:

[source,xml]
----
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-config</artifactId>
</dependency>
----

然后还需要添加 `bootstrap.properties` （或 `bootstrap.yml` ） 文件，指定读取配置的方式。

读取配置的方式支持两种:

- <<url_read_config>>
- <<discovery_read_config>>

[#url_read_config]
==== URL方式读取配置

默认情况下，配置中心采用的是URL的方式读取配置。

这种方式下，客户端直接通过URL读取配置中心的参数，以下是 `bootstrap.properties` 的示例：

[source,properties]
.bootstrap.properties
----
spring.cloud.config.uri=http://localhost/api/config-server // <1>
spring.application.name=TestProject_TestApp // <2>
spring.profiles.active=TestEnv // <3>
----
<1> 配置中心URL
<2> 项目编号_应用编号
<3> 环境编号

[#discovery_read_config]
==== 注册发现方式读取配置

在微服务架构中，两个服务注册到同一个注册/发现中心时，即可通过注册中心获取调用地址。

这时候，可以设置配置中心的参数 `DISCOVERY_TYPE` 为对应的注册中心，并设置对应的连接信息，例如：

[source,yaml]
.docker-compose.yml
----
...
DISCOVERY_TYPE: eureka
EUREKA_SERVICE_URL: http://admin:admin@localhost:8761/eureka/
----

此时配置中心即可注册到注册中心，客户端也需要注册到同一个注册中心，并通过服务名读取配置中心中的参数：

[source,properties]
.bootstrap.properties
----
spring.application.name=TestProject_TestApp // <1>
spring.profiles.active=TestEnv // <2>
spring.cloud.config.discovery.enable=true // <3>
spring.cloud.config.discovery.service-id=config-server // <4>
eureka.client.service-url.defaultZone=http://admin:admin@localhost:8761/eureka/ // <5>
----
<1> 项目编号_应用编号
<2> 环境编号
<3> 开启注册中心读取配置
<4> 配置中心的的服务名
<5> 注册中心地址

[#notice_manage]
=== 推送通知

在开发中有时候需要在运行(runtime)期间动态的调整某些参数，这时就可以使用推送通知功能。

推送通知功能使用rabbitmq队列，在使用推送通知功能之前需要预先通过 `MONITOR_TYPE` 参数（<<backend_appendix>>）开启推送通知功能，默认情况下 `MONITOR_TYPE` 的值为 `none`，即不开启推送通知功能。我们需要将之设置为 `rabbitmq` 即可开启。

开启推送通知功能后，接下来需要配置客户端。

客户端首先需要添加推送依赖:

[source,xml]
----
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-bus-amqp</artifactId>
</dependency>
----

还需要设置 rabbitmq 连接参数，我们需要将配置中心和客户端连接到同一个 `Exchange` 中，这样才能接收到配置中心推送的通知，默认情况下配置中心使用 `config-server-exchange` 作为 `Exchange` 名称。

示例配置：

[source,properties]
----
spring.rabbitmq.username=admin
spring.rabbitmq.password=admin
spring.rabbitmq.host=localhost
spring.rabbitmq.port=5672
spring.cloud.stream.bindings.springCloudBusOutput.destination=config-server-exchange
spring.cloud.stream.bindings.springCloudBusInput.destination=config-server-exchange
----

这些配置也可以在配置中心中提前配置好，然后将它和应用进行关联。

最后还需要加入 `@RefreshScope` 注解，该注解可以标记任意 `Bean` ，之后该 bean 就会加入到运行期间自动刷新的生命周期中。

NOTE: `@RefreshScope` 可能会导致 bean 重新实例化，出现BUG时请参考这一点解决。

NOTE: 许多启动时读取的参数无法通过推送通知动态的改变，例如 `server.port` 。这些参数需要重启服务来使之生效


[#keyboard_shortcut]
=== 快捷键

为了方便快速管理配置中心，于是加入了快捷键功能。

以下为一些常用快捷键的列表，更多快捷键请在配置中心中的任意页面按下 `?` 建查看提示：

.常用快捷键列表
[%autowidth]
|===
| 快捷键 | 说明 | 备注
| `Alt+0` | 跳转到 `项目列表` |
| `Alt+1` | 跳转到 `应用列表` |
| `Alt+2` | 跳转到 `环境列表` |
| `Alt+3` | 跳转到 `配置列表` |
| `Alt+4` | 跳转到 `通知列表` |
| `Alt+N` | 跳转到 `新建页面` | 例如在 `项目列表` 页面使用，就可以跳转到 `新建项目` 页面，某些页面可以没有该快捷键
| `Alt+L` | 跳转到 `列表页面` | 例如在 `新建项目` 页面使用，就可以跳转到 `项目列表` 页面，某些页面可以没有该快捷键
|===

NOTE: 在 `MacOS` 系统下，使用 `Option` 代替 `alt`

//
//=== 创建配置并关联应用
//
//为了适配多项目，所以进入页面后首先需要创建一个项目。
//
//创建项目后可以根据需求创建对应的环境，以及每个环境中的配置。应用则代表具体的服务，在应用详情中将应用和配置进行关联，之后就可以进行客户端连接了。
//
//=== 客户端连接配置中心读取配置
//
//首先客户端需要在 `pom.xml` 中添加依赖:
//
//----
//<dependency>
//    <groupId>org.springframework.cloud</groupId>
//    <artifactId>spring-cloud-starter-config</artifactId>
//</dependency>
//----
//
//NOTE: 该依赖的版本号应该从 SpringCloudDependencies 中继承
//
//然后创建 `bootstrap.yml` 文件:
//
//NOTE: `bootstrap.yml` 文件只在示例中使用，实际开发/部署时可以根据需求自行配置，只需在 `spring.profiles.active` 中包含对应的环境编号即可。
//
//----
//spring:
//  cloud:
//    config:
//      uri: http://localhost <!--1-->
//  application:
//    name: TestProject_UserService <!--2-->
//  profiles:
//    active: ProductionEnv <!--3-->
//----
//<1> 默认情况下，配置中心不使用任何注册中心，所以客户端应该使用 `地址+端口号` 的方式直连配置中心。
//<2> 使用 `项目编号_应用编号` 的方式区分项目和应用
//<3> `环境编号`
//
//之后启动客户端可以发现，已经从配置中心中读取了对应的配置:
//
//[source%nowrap]
//----
//  .   ____          _            __ _ _
// /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
//( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
// \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
//  '  |____| .__|_| |_|_| |_\__, | / / / /
// =========|_|==============|___/=/_/_/_/
// :: Spring Boot ::        (v2.3.1.RELEASE)
//
//2020-06-18 15:40:45.422  INFO 61911 --- [           main] c.c.c.ConfigServicePropertySourceLocator : Fetching config from server at : http://localhost
//2020-06-18 15:40:45.553  WARN 61911 --- [           main] c.c.c.ConfigServicePropertySourceLocator : Could not locate PropertySource: Could not extract response: no suitable HttpMessageConverter found for response type [class org.springframework.cloud.config.environment.Environment] and content type [text/html]
//2020-06-18 15:40:45.556  INFO 61911 --- [           main] com.example.demo.DemoApplication         : The following profiles are active: ProductionEnv
//2020-06-18 15:40:46.205  INFO 61911 --- [           main] o.s.cloud.context.scope.GenericScope     : BeanFactory id=9451707b-647f-3f81-8517-6d49f66f4a74
//2020-06-18 15:40:46.405  INFO 61911 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 30006 (http)
//2020-06-18 15:40:46.413  INFO 61911 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
//2020-06-18 15:40:46.413  INFO 61911 --- [           main] org.apache.catalina.core.StandardEngine  : Starting Servlet engine: [Apache Tomcat/9.0.36]
//2020-06-18 15:40:46.494  INFO 61911 --- [           main] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
//2020-06-18 15:40:46.495  INFO 61911 --- [           main] w.s.c.ServletWebServerApplicationContext : Root WebApplicationContext: initialization completed in 928 ms
//2020-06-18 15:40:46.787  INFO 61911 --- [           main] o.s.s.concurrent.ThreadPoolTaskExecutor  : Initializing ExecutorService 'applicationTaskExecutor'
//2020-06-18 15:40:47.180  INFO 61911 --- [           main] o.s.b.a.e.web.EndpointLinksResolver      : Exposing 2 endpoint(s) beneath base path '/actuator'
//2020-06-18 15:40:47.264  INFO 61911 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 30006 (http) with context path ''
//2020-06-18 15:40:47.279  INFO 61911 --- [           main] com.example.demo.DemoApplication         : Started DemoApplication in 2.716 seconds (JVM running for 3.584)
//2020-06-18 15:40:47.539  INFO 61911 --- [on(1)-127.0.0.1] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring DispatcherServlet 'dispatcherServlet'
//2020-06-18 15:40:47.539  INFO 61911 --- [on(1)-127.0.0.1] o.s.web.servlet.DispatcherServlet        : Initializing Servlet 'dispatcherServlet'
//2020-06-18 15:40:47.542  INFO 61911 --- [on(2)-127.0.0.1] c.c.c.ConfigServicePropertySourceLocator : Fetching config from server at : http://localhost
//2020-06-18 15:40:47.548  INFO 61911 --- [on(1)-127.0.0.1] o.s.web.servlet.DispatcherServlet        : Completed initialization in 9 ms
//2020-06-18 15:40:47.549  WARN 61911 --- [on(2)-127.0.0.1] c.c.c.ConfigServicePropertySourceLocator : Could not locate PropertySource: Could not extract response: no suitable HttpMessageConverter found for response type [class org.springframework.cloud.config.environment.Environment] and content type [text/html]
//----
//
//=== 推送通知功能
//
//推送通知功能可以在配置发生改变后，实时的通知客户端动态的读取最新配置。
//
//使用推送通知功能需要将服务器端和客户端连接到同一个 rabbitmq 队列，然后依赖 `spring-cloud-starter-bus-amqp` ，这样即可在配置更新时通过队列通知客户端进行改变。
//
//NOTE: 许多启动时读取的参数无法通过推送通知动态的改变，例如 `server.port`
//
//==== 服务器端配置
//
//服务器端配置 `MONITOR_TYPE` 参数为 `"rabbitmq"` ，并配置rabbitmq的必要参数 `RABBITMQ_HOST` 、`RABBITMQ_PORT`、`RABBITMQ_USERNAME`、`RABBITMQ_PASSWORD` 之后，即可开启推送通知功能，自动注册到 rabbitmq。
//
//==== 客户端配置
//
//客户端添加依赖:
//
//[source,xml]
//----
//<dependency>
//    <groupId>org.springframework.cloud</groupId>
//    <artifactId>spring-cloud-starter-bus-amqp</artifactId>
//</dependency>
//----
//
//之后将 `spring.cloud.stream.bindings.springCloudBusOutput.destination` 、 `spring.cloud.stream.bindings.springCloudBusInput.destination` 配置为和服务器端相同（服务器端默认为 `config-server-exchange`），即可和服务器端注册到同一个 rabbitmq队列中。
